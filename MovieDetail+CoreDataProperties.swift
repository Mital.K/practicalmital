//
//  MovieDetail+CoreDataProperties.swift
//  PracticalTatva
//
//  Created by SOTSYS129 on 11/01/21.
//  Copyright © 2021 MitalK. All rights reserved.
//
//

import Foundation
import CoreData


extension MovieDetail {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MovieDetail> {
        return NSFetchRequest<MovieDetail>(entityName: "MovieDetail")
    }

    @NSManaged public var id: String?
    @NSManaged public var overview: String?
    @NSManaged public var genres: String?
    @NSManaged public var duration: String?
    @NSManaged public var releaseDate: String?
    @NSManaged public var prodCompany: String?
    @NSManaged public var prodBudget: String?
    @NSManaged public var revenue: String?
    @NSManaged public var lang: String?
    @NSManaged public var posterPath: String?
    @NSManaged public var name: String?
    @NSManaged public var backPath: String?
    @NSManaged public var tagline: String?

}
