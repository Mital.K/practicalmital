//
//  MovieList+CoreDataProperties.swift
//  PracticalTatva
//
//  Created by SOTSYS129 on 11/01/21.
//  Copyright © 2021 MitalK. All rights reserved.
//
//

import Foundation
import CoreData


extension MovieList {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MovieList> {
        return NSFetchRequest<MovieList>(entityName: "MovieList")
    }

    @NSManaged public var date: String?
    @NSManaged public var imgUrl: String?
    @NSManaged public var overview: String?
    @NSManaged public var title: String?
    @NSManaged public var detailId: String?

}
