//
//  NSObject+Helpers.swift
//  PracticalTatva
//
//  Created by SOTSYS129 on 11/01/21.
//  Copyright © 2021 MitalK. All rights reserved.
//

import Foundation


extension NSObject {
    
    var className : String {
        return String(describing: self)
    }
    
    static var className : String {
        return String(describing: self)
    }
}
extension String{
    var convertDateTimeFormate:String{
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let showDate = inputFormatter.date(from: self)
        inputFormatter.dateFormat = "dd MMM. yyyy hh:mm a"
        let resultString = inputFormatter.string(from: showDate!)
        return resultString
    }
}
