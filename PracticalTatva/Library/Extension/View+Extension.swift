//
//  View+Extension.swift
//  PracticalTatva
//
//  Created by SOTSYS129 on 11/01/21.
//  Copyright © 2021 MitalK. All rights reserved.
//

import UIKit
import SystemConfiguration

extension UIViewController {
    
    var checkInternet:Bool{
        
        if isInternetAvailable{
            return true
        }else{
            self.showAlert(appName,"No internet connection!")
             return false
        }
    }
    var isInternetAvailable:Bool{
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    func showAlert(_ title : String = appName, _ message:String){
           
           let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
           let cancelAction: UIAlertAction = UIAlertAction(title: "Ok", style: .cancel) { action -> Void in
               
           }
           alertController.addAction(cancelAction)
           self.present(alertController, animated: true, completion: nil)
       }
}
