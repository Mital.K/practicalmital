//
//  MovieListTblCell.swift
//  PracticalTatva
//
//  Created by SOTSYS129 on 11/01/21.
//  Copyright © 2021 MitalK. All rights reserved.
//

import UIKit
import SDWebImage

class MovieListTblCell: UITableViewCell {

    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    var modelData : MovieList?{
        didSet{
            self.setModelData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func setModelData(){
        lblTitle.text = modelData?.title
        lblDate.text = modelData?.date
        lblDescription.text = modelData?.overview
        if let imageurl = modelData?.imgUrl{
            let path = Constant.ImageBasePath + imageurl
            imgCover.sd_setImage(with: URL(string:path),placeholderImage:#imageLiteral(resourceName: "placeholder"))
        }
    }
}
