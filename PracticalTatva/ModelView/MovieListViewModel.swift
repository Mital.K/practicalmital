//
//  MovieListViewModel.swift
//  PracticalTatva
//
//  Created by SOTSYS129 on 11/01/21.
//  Copyright © 2021 MitalK. All rights reserved.
//

import UIKit
import CoreData


class MovieListViewModel: NSObject {

    var movieData = [MovieList]()
    var movieDetail = MovieDetail()
    var refreshData:(()->Void)?
    var refreshDetailData:(()->Void)?
    
    func getDataFromServer(){
        
        DispatchQueue.main.async {
            Service.sharedInstance.getMovieList(page: 0) { (isCuccess, error, data) in
                if isCuccess{
                    if let data = data?.results{
                        self.getDataFromDB()
                    }
                }else{
                    print(error.debugDescription)
                }
            }
        }
        
    }
    func getMovieDetailFromServer(byid:String){
           
           DispatchQueue.main.async {
               Service.sharedInstance.getMovieDetail(id:byid) { (isCuccess, error, data) in
                   if isCuccess{
                       if let data = data{
                           self.getMovieDetailFromDB(id: byid)
                       }
                   }else{
                       print(error.debugDescription)
                   }
               }
           }
           
    }
    func getMovieDetailFromDB(id:String){
        if let data = Service.sharedInstance.getMovieDetailDB(id: id){
            movieDetail = data
            refreshDetailData?()
        }
      
    }
    func getDataFromDB(){
        let array = Service.sharedInstance.getMovieListDB()
        if let arr = array{
            movieData = arr
            refreshData?()
        }
    }
}
    
