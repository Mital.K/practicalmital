//
//  AppDelegate.swift
//  PracticalTatva
//
//  Created by SOTSYS129 on 11/01/21.
//  Copyright © 2021 MitalK. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {


    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        let path = NSSearchPathForDirectoriesInDomains(.applicationDirectory, .userDomainMask, true)
        print(path)
        return true
    }
   
    
    static var shared : AppDelegate {
           return UIApplication.shared.delegate as! AppDelegate
    }
}

